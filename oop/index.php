<?php
    require_once 'animal.php';
    require_once 'Frog.php';
    require_once 'Ape.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP</title>
</head>
<body>
    <?php
        $sheep = new Animal("shaun");

        echo "Name : " . $sheep->name . "<br>"; // "shaun"
        echo "Legs : " . $sheep->legs . "<br>"; // 4
        echo "Cold blooded : " . $sheep->cold_blooded . "<br>"; // "no"

        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

        $kodok = new Frog("buduk");

        echo "<br>Name : " . $kodok->name . "<br>"; 
        echo "Legs : " . $kodok->legs . "<br>"; 
        echo "Cold blooded : " . $kodok->cold_blooded . "<br>"; 
        echo "Jump : " . $kodok->jump . "<br>";
        
        $sungokong = new Ape("Kera Sakti");

        echo "<br>Name : " . $sungokong->name . "<br>"; 
        echo "Legs : " . $sungokong->legs . "<br>"; 
        echo "Cold blooded : " . $sungokong->cold_blooded . "<br>"; 
        echo "Yell : " . $sungokong->yell . "<br>";
    ?>

</body>
</html>