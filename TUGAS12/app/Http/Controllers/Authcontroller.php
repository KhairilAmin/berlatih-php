<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Authcontroller extends Controller
{
    public function register(){
        return view('register');
    }

    public function kirim(Request $data){
        $namadepan = $data['firstname'];
        $namaakhir = $data['lastname'];

        return view('home', compact('namadepan','namaakhir')); 
    }
}
