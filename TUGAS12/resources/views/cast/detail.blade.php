@extends('layout.master')
@section('judul')
    Detail Cast
@endsection
@section('isi')
    <h1 class="text-primary">{{$cast->nama}}</h1>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>    
@endsection