@extends('layout.master')
@section('judul')
    Halaman Data Cast
@endsection

@section('isi')
<a href="/cast/create" class="btn btn-primary btn-sm mb-4">Tambah Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Detail</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @method('delete')
                        @csrf
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data di tabel cast</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection